import {gDevcampReact, tyLeSinhVienTheoHoc} from './info'

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} width="500px"/>
      <p>Tỷ lệ sinh viên theo học: {tyLeSinhVienTheoHoc()}%</p>
      {tyLeSinhVienTheoHoc() > 15 ? "Sinh viên đăng ký học nhiều" : "Sinh viên đăng ký học ít"}
      <p>Các ưu điểm của môn học Reactjs</p>
      <ul>
        {
          gDevcampReact.benefits.map((value, index) => {
            return <li key={index}>{value}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
