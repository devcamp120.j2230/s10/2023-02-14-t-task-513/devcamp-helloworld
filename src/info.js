import image from './assets/images/logo-og.png'

const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image,
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 10,
    totalStudents: 100
  }
  
  const tyLeSinhVienTheoHoc = () => gDevcampReact.studyingStudents * 100 / gDevcampReact.totalStudents;

  export {gDevcampReact, tyLeSinhVienTheoHoc}